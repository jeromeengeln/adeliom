<?php 

namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class TagController extends Controller
{
    /**
     * @Route("/admin/tags", name="tags")
     */
    public function tagsList(Request $request) {

        $repository = $this->getDoctrine()
        ->getRepository(Tag::class);

        $query = $repository->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->getQuery();

        $tags = $query->getResult();

        return $this->render('tags/tags.list.twig', [
            'tags' => $tags
        ]);

    }

    /**
     * @Route("/admin/tags/new", name="tag_new")
     */
    public function newTag(Request $request) {
        // creates a task and gives it some dummy data for this example
        $tag = new Tag();

        $form = $this->createFormBuilder($tag)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Validate'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $tag = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tag);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Tag saved!'
            );

            return $this->redirectToRoute('tag_new');
        }

        return $this->render('tags/tag.form.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/tags/edit/{{id}}", name="tag_edit")
     */
    public function editTag($id, Request $request) {

        // creates a task and gives it some dummy data for this example
        $tag = $this->getDoctrine()
        ->getRepository(Tag::class)
        ->find($id);

        $form = $this->createFormBuilder($tag)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Modify'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $tag = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tag);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Tag updated!'
            );

            return $this->redirectToRoute('tag_edit', ['id' => $id]);
        }

        return $this->render('tags/tag.form.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/tags/delete/{{id}}", name="tag_delete")
     */
    public function deleteTag($id, Request $request) {

        $tag = $this->getDoctrine()
        ->getRepository(Tag::class)
        ->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tag);
        $entityManager->flush();

        return $this->redirectToRoute('tags');
        
    } 

}


?>