<?php 

namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NavController extends Controller
{
    /**
     * @Route("/admin/tags", name="tags")
     */
    public function tagsList(Request $request) {

        $repository = $this->getDoctrine()
        ->getRepository(Tag::class);

        $query = $repository->createQueryBuilder('p')
            ->orderBy('p.name', 'ASC')
            ->getQuery();

        $tags = $query->getResult();

        return $this->render('tags/tags.list.twig', [
            'tags' => $tags
        ]);

    }

    /**
     * @Route("/admin/tags/new", name="tag_new")
     */
    public function newTag(Request $request) {
        // creates a task and gives it some dummy data for this example
        $tag = new Tag();

        $form = $this->createFormBuilder($tag)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Validate'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $tag = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tag);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Tag saved!'
            );

            return $this->redirectToRoute('tag_new');
        }

        return $this->render('tags/tag.form.twig', array(
            'form' => $form->createView(),
        ));
    }
}

?>