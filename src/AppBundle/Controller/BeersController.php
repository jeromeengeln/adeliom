<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

// ajout des modeles de données
use AppBundle\Entity\Beer;
use AppBundle\Entity\Tag;
use AppBundle\Entity\Brand;
use AppBundle\Entity\User;

class BeersController extends Controller
{

    /**
     * @Route("/", name="beersfront")
     */
    public function beersFront(Request $request)
    {
        
        return $this->render('beers/beers.front.twig', [
            'beers' => $this->getBeers()
        ]);

    }

    /**
     * @Route("/admin/", name="beers")
     */
    public function beersBack(Request $request)
    {
        
        return $this->render('beers/beers.back.twig', [
            'beers' => $this->getBeers()
        ]);

    }

    public function getBeers() {

        $repository = $this->getDoctrine()
        ->getRepository(Beer::class)
        ->findAll();

        return $repository;

    }

    private function getTags() {

        $repository = $this->getDoctrine()
        ->getRepository(Tag::class);

        $query = $repository->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->getQuery();

        return $query->getResult();

    }

    /**
     * @Route("/admin/beers/new", name="beer_new")
     */
    public function newBeer(Request $request) {
        // creates a task and gives it some dummy data for this example
        $beer = new Beer();
        $error = '';

        $tags = $this->getTags();

        $form = $this->createFormBuilder($beer)
            ->add('name', TextType::class)
            ->add('brand_id', EntityType::class, array( 'label'=>'Brand', 'class' => 'AppBundle:Brand', 'choice_label'=>'name' ))
            ->add('user_id', EntityType::class, array( 'label'=>'User', 'class' => 'AppBundle:User', 'choice_label'=>'username' ))
            ->add('tags', EntityType::class, array( 'label'=>'Linked tags', 'class' => 'AppBundle:Tag', 'choice_label'=>'name', 'multiple' => true, 'expanded' => true ))
            ->add('description', TextareaType::class, [ 'label'=>'Description' ])
            ->add('save', SubmitType::class, array('label' => 'Validate'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $beer = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($beer);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Beer updated!'
            );

            return $this->redirectToRoute('beers');

        }

        return $this->render('beers/beer.form.twig', array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/admin/beers/edit/{{id}}", name="beer_edit")
     */
    public function editUser($id, Request $request) {

        // creates a task and gives it some dummy data for this example
        $beer = $this->getDoctrine()
        ->getRepository(Beer::class)
        ->find($id);
        $error = '';

        $user = $this->getDoctrine()
        ->getRepository(User::class)
        ->find($beer->user_id());
        $beer->setUser_id($user);

        $brand = $this->getDoctrine()
        ->getRepository(Brand::class)
        ->find($beer->brand_id());
        $beer->setBrand_id($brand);

        $form = $this->createFormBuilder($beer)
            ->add('name', TextType::class)
            ->add('brand_id', EntityType::class, array( 'label'=>'Brand', 'class' => 'AppBundle:Brand', 'choice_label'=>'name' ))
            ->add('user_id', EntityType::class, array( 'label'=>'User', 'class' => 'AppBundle:User', 'choice_label'=>'username' ))
            ->add('tags', EntityType::class, array( 'label'=>'Linked tags', 'class' => 'AppBundle:Tag', 'choice_label'=>'name', 'multiple' => true, 'expanded' => true ))
            ->add('description', TextareaType::class, [ 'label'=>'Description' ])
            ->add('save', SubmitType::class, array('label' => 'Modify'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $beer = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($beer);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Beer updated!'
            );

            return $this->redirectToRoute('beer_edit', ['id' => $id]);
 
        }

        return $this->render('beers/beer.form.twig', array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/admin/beers/delete/{{id}}", name="beer_delete")
     */
    public function deleteBeer($id, Request $request) {

        $beer = $this->getDoctrine()
        ->getRepository(Beer::class)
        ->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($beer);
        $entityManager->flush();

        return $this->redirectToRoute('beers');
        
    } 


}
