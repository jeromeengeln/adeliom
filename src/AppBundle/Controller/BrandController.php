<?php 

namespace AppBundle\Controller;

use AppBundle\Entity\Brand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class BrandController extends Controller
{
    /**
     * @Route("/admin/brands", name="brands")
     */
    public function brandsList(Request $request) {

        $repository = $this->getDoctrine()
        ->getRepository(Brand::class);

        $query = $repository->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->getQuery();

        $brands = $query->getResult();

        return $this->render('brands/brands.list.twig', [
            'brands' => $brands
        ]);

    }

    /**
     * @Route("/admin/brand/new", name="brand_new")
     */
    public function newBrand(Request $request) {
        // creates a task and gives it some dummy data for this example
        $brand = new Brand();

        $form = $this->createFormBuilder($brand)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Validate'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $brand = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($brand);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Brand saved!'
            );

            return $this->redirectToRoute('brand_new');
        }

        return $this->render('brands/brand.form.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/brands/edit/{{id}}", name="brand_edit")
     */
    public function editBrand($id, Request $request) {

        // creates a task and gives it some dummy data for this example
        $brand = $this->getDoctrine()
        ->getRepository(Brand::class)
        ->find($id);

        $form = $this->createFormBuilder($brand)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Modify'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $brand = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($brand);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Brand updated!'
            );

            return $this->redirectToRoute('brand_edit', ['id' => $id]);
        }

        return $this->render('brands/brand.form.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/brands/delete/{{id}}", name="brand_delete")
     */
    public function deleteBrand($id, Request $request) {

        $brand = $this->getDoctrine()
        ->getRepository(Brand::class)
        ->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($brand);
        $entityManager->flush();

        return $this->redirectToRoute('brands');
        
    } 

}


?>