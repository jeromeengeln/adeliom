<?php 

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends Controller
{
    /**
     * @Route("/admin/users", name="users")
     */
    public function usersList(Request $request) {

        $repository = $this->getDoctrine()
        ->getRepository(User::class);

        $query = $repository->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->getQuery();

        $users = $query->getResult();

        return $this->render('users/users.list.twig', [
            'users' => $users
        ]);

    }

    /**
     * @Route("/admin/users/new", name="user_new")
     */
    public function newUser(Request $request, UserPasswordEncoderInterface $encoder) {
        // creates a task and gives it some dummy data for this example
        $user = new User();
        $error = '';

        $form = $this->createFormBuilder($user)
            ->add('username', TextType::class)
            ->add('email', EmailType::class)
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ))
            ->add('save', SubmitType::class, array('label' => 'Validate'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $entityManager = $this->getDoctrine()->getManager();

            // User existant ?
            $query = $entityManager->createQuery(
                'SELECT p
                FROM AppBundle:User p
                WHERE  (p.email = :email OR p.username = :username) '
            )
            ->setParameter('email', $user->getEmail())
            ->setParameter('username', $user->getUserName());
            $result = $query->getResult();

            if(empty($result)) {

                $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($encoded);

                // ... perform some action, such as saving the task to the database
                // for example, if Task is a Doctrine entity, save it!
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash(
                    'notice',
                    'User saved!'
                );

                return $this->redirectToRoute('user_new');

            } else {

                $error = 'User already exists';

            } 
        }

        return $this->render('users/user.form.twig', array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/admin/users/edit/{{id}}", name="user_edit")
     */
    public function editUser($id, Request $request, UserPasswordEncoderInterface $encoder) {

        // creates a task and gives it some dummy data for this example
        $user = $this->getDoctrine()
        ->getRepository(User::class)
        ->find($id);
        $error = '';

        $form = $this->createFormBuilder($user)
            ->add('username', TextType::class)
            ->add('email', EmailType::class)
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array('label' => 'New password'),
                'second_options' => array('label' => 'Repeat Password'),
            ))
            ->add('save', SubmitType::class, array('label' => 'Modify'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $user = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();

            // User existant ?
            $query = $entityManager->createQuery(
                'SELECT p
                FROM AppBundle:User p
                WHERE  (p.email = :email OR p.username = :username) AND p.id != :id'
            )
            ->setParameter('email', $user->getEmail())
            ->setParameter('username', $user->getUserName())
            ->setParameter('id', $user->getId());
            $result = $query->getResult();

            if(empty($result)) {

                $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($encoded);

                // ... perform some action, such as saving the task to the database
                // for example, if Task is a Doctrine entity, save it!
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash(
                    'notice',
                    'User updated!'
                );

                return $this->redirectToRoute('user_edit', ['id' => $id]);

            } else {

                $error = 'User already exists';

            } 

            
        }

        return $this->render('users/user.form.twig', array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/admin/users/delete/{{id}}", name="user_delete")
     */
    public function deleteUser($id, Request $request) {

        $user = $this->getDoctrine()
        ->getRepository(User::class)
        ->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute('users');
        
    } 

}


?>