<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Brand;
use AppBundle\Entity\Tag;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="beers")
 */
class Beer
{   
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Brand")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $brand_id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $user_id;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tag")
     * @ORM\JoinTable(name="beers_tags",
     *      joinColumns={@ORM\JoinColumn(name="beer_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     * @Assert\NotBlank()
     */
    private $tags;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function __contruct() 
    {

        $this->tags = new ArrayCollection();

    }

    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;
    }

    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    public function removeTag(Tag $tag)
    {
        // Ici on utilise une méthode de l'ArrayCollection, pour supprimer la catégorie en argument
        $this->tags->removeElement($tag);
    }

    // Notez le pluriel, on récupère une liste de catégories ici !
    public function getTags()
    {   
        return $this->tags;
    }

    public function getId() {
        return $this->id;
    }
    
    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }
    
    public function setName($name) {
        $this->name = $name;
    }

    public function __get($name) {
        return $this->$name;
    }
    
    public function __set($name, $val) {
        $this->$name = $val;
    }

    public function brand_id() {
        return $this->brand_id;
    }
    
    public function setBrand_Id($val) {
        $this->brand_id = $val;
    }
    
    public function setUser_Id($val) {
        $this->user_id = $val;
    }

    public function user_id() {
        return $this->user_id;
    }

    public function getDescription() {
        return $this->description;
    }
    
    public function setDescription($val) {
        $this->description = $val;
    }

}

?>