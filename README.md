project_test Adeliom
============

A Symfony project created on April 24, 2018, 7:56 am.
Jérôme Engeln

Topo / todo :
====

Partie front :

- Lister et pouvoir consulter des fiches de bières avec toutes les informations les concernants (nom, image, description mise en forme, marque, auteur de la fiche, tags, etc.).

Partie gestion :

- Gérer des marques de bières : Fischer, Météor etc.
ok

- Gérer des auteurs des fiches : Utilisateur 1, 2, 3.
ok

- Gérer des tags : Tag1, Tag2, Tag3
ok

- Gérer des fiches bières (Bière de printemps, Bière de Noël etc.) et y associer une marque, un auteur et des tags.
ok

- Gérer = ajouter, modifier, supprimer.
ok

Contraintes :

- Utiliser Symfony dans sa version 3.4 (ce sera plus intéressant pour voir le process global).
ok

- Responsive.
ok avec bootstrap 4

- Utiliser une base de données externe type MySQL.
ok

- Mettre sur un serveur pour consulter en ligne + envoyer les sources.
à faire


Doc :
====

https://symfony.com/doc/3.4/index.html

install project :
====
https://symfony.com/doc/3.4/setup.html
- composer update
- yarn install
- php bin/console doctrine:schema:update --force
- php bin/console server:run
- yarn run encore dev --watch


local php serveur
====
- php bin/console server:run

db :
====

- php bin/console doctrine:schema:update --dump-sql
- php bin/console doctrine:schema:update --force
- php bin/console doctrine:schema:validate

- php bin/console doctrine:database:create
- php bin/console doctrine:database:drop --force

yarn compile assets (js + scss) :
====

- yarn run encore dev --watch