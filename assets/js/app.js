import "../css/variables.scss";
import "../css/app.scss";

import $ from "jquery";
import "bootstrap/dist/js/bootstrap";

$(function() {

    $('.delete').click(function(event) {
        event.stopPropagation();
        if(confirm('Please confirm !')) {
            return true;
        }
        return false;
    });

    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var descriptif = button.next().text() // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-body').text(descriptif)
    })

});